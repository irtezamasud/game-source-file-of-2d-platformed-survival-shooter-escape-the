﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rocketHit : MonoBehaviour {

    public float weaponDamage;

    projectileController myPC;

    public GameObject explosionEffect;

	// Use this for initialization
	void Awake () {
        myPC = GetComponentInParent<projectileController>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter2D(Collider2D other)   //shooting the object named as shootable
    {
        if(other.gameObject.layer == LayerMask.NameToLayer("Shootable"))
        {
            myPC.removeForce();
            Instantiate(explosionEffect, transform.position, transform.rotation);
            Destroy(gameObject);                //this destroys rocket.But,keep smoketrails
            if (other.tag == "Enemy")
            {
                enemyHealth hurtEnemy = other.gameObject.GetComponent<enemyHealth>();  //made a variable for accessing in enemy health script
                hurtEnemy.addDamage(weaponDamage);
            }

        }
    }
    void OnTriggerStay2D(Collider2D other)   //safeguard
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Shootable"))
        {
            myPC.removeForce();
            Instantiate(explosionEffect, transform.position, transform.rotation);
            Destroy(gameObject);                //this destroys rocket.But,keep smoketrails
            if (other.tag == "Enemy")
            {
                enemyHealth hurtEnemy = other.gameObject.GetComponent<enemyHealth>();  //made a variable for accessing in enemy health script
                hurtEnemy.addDamage(weaponDamage);
            }

        }
    }
}
