﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    //movement variable
    public float maxSpeed = 8;

    //jumping variable
    bool grounded = false;
    float groundCheckRadius = 0.2f;
    public LayerMask groundLayer;
    public Transform groundCheck;
    public float jumpHeight;

    

    Rigidbody2D myRB;
    Animator myAnim;
    bool facingright;

    //for shooting rocket

    public Transform gunTip;
    public GameObject bullet;
    float fireRate = 0.5f;
    float nextFire = 0f;

	// Use this for initialization
	void Start () {
        myRB = GetComponent<Rigidbody2D>();                                     //By get component we can look for some specific things
        myAnim = GetComponent<Animator>();

        facingright = true;
	}

    // Update is called once per frame

    void Update()
    {
        if (grounded && Input.GetAxis("Jump") > 0)
        {
            grounded = false;
            myAnim.SetBool("isGrounded", grounded);
            myRB.AddForce(new Vector2(0, jumpHeight));                       //jumping in the Y direction
        }
            //player firing
            if (Input.GetAxisRaw("Fire1") > 0) fireRocket();

        
    }
    void FixedUpdate () {

        //check if we are grounded.If no! then we are falling
        grounded = Physics2D.OverlapCircle(groundCheck.position,groundCheckRadius,groundLayer);          //draw a circle below the player
        myAnim.SetBool("isGrounded", grounded);

        myAnim.SetFloat("verticalSpeed", myRB.velocity.y);

        float move = Input.GetAxis("Horizontal");                          //Get axis return a value between 1 and negative 1
        myAnim.SetFloat("speed", Mathf.Abs(move));

        myRB.velocity = new Vector2(move * maxSpeed, myRB.velocity.y);      //multiply input value and max speed to move the physics object


        if(move>0 && !facingright)                                          //this if condition is made for making the character flip
        {
            flip();
        }else if (move<0 && facingright)
        {
            flip();
        }
	}

    //useful function 

    void flip()
    {
        facingright = !facingright;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;                                                   //taking the x value and multiply by negative 1
        transform.localScale = theScale;                                       //Put the final transform in the variable
    }

    void fireRocket()                                                         //rocket function
    {
        if (Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            if (facingright)                            //rocket physical property
            {
                Instantiate(bullet, gunTip.position, Quaternion.Euler (new Vector3 (0, 0, 0))); //locking the rocket rotation
            }else if (!facingright)

            {
                Instantiate(bullet, gunTip.position, Quaternion.Euler(new Vector3(0, 0, 180f)));
            }
            
        }
    }
}
