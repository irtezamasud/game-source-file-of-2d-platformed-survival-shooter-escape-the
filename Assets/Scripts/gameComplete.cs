﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameComplete : MonoBehaviour {

    public AudioClip portalReady;
    AudioSource PR;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        AudioSource.PlayClipAtPoint(portalReady, transform.position);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            playerHealth playerWins = other.gameObject.GetComponent<playerHealth>();
            playerWins.winGame();
        }
    }
}
